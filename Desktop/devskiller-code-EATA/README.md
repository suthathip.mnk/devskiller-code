## Introduction

This application uses`UserModel`, `PostModel` and `CommentModel` models. You must implement an Index action for `posts` controller so that posts can be rendered appropriately into the `Posts/Index` view. This view requires comments to be implemented as well. The steps to accomplish this challenge are as follows:

## Problem Statement

### 1) Fix routing

Modify `RouteConfig.cs` to make sure that  `/Posts` routes to Index action of `PostsController`

### 2) Implement index action for post in `PostsController.cs` and `CommentDAL.cs`

Make sure that Index action of `PostsController` takes *two* parameters: `title` and `sortOrder` and use them in the appropirate way.

When `title` is passed into `action`, only posts that contains that phrase in their title should be returned. Title search schould be case insensitive.
If there is no `title` passed into `action`, or it's empty, all posts should be returned.

If `sortOrder` is passed, it can have one of two string values: "oldest" or "newest". Returned posts should be sorted by their value, to bring the oldest or newest post to the top.
If `sortOrder` is not passed into `action`, it should by default sort by the newest posts first.

The data that is needed by `Views/Posts/Index.cshtml` should be returned by `action`.
Comments should be sorted by creation time: newest posts first.

## Hints

To **run integration tests** on your local environment, you may be required to run them as `administrator` or/and in Visual Studio go to `Tools` | `Options` | `Test` | `General` and uncheck the `For improved performance, only use test adapters in test assembly folders or as specified in runsettings file` checkbox.
