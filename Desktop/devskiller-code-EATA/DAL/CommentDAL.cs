using EL;
using EL.Models;
using System.Collections.Generic;
using System.Linq;

namespace DAL
{
    public class CommentDAL
    {
        private readonly PostsContext _dbContext;

        public CommentDAL(PostsContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<PostModel> GetPosts(string title = null, string sortOrder = null)
        {
            var posts = _dbContext.Posts
                .Select(x => new PostModel
                {
                    Id = x.PostId,
                    UserId = x.UserId,
                    Title = x.Title,
                    Content = x.Content,
                    Comments = x.Comments.Select(y => new CommentModel
                    {
                        UserId = y.UserId,
                        PostId = y.PostId,
                        Content = y.Content,
                        CreatedAt = y.CreatedAt,
                        UpdatedAt = y.UpdatedAt
                    }),
                    CreatedAt = x.CreatedAt,
                    UpdatedAt = x.UpdatedAt
                });

            if (!string.IsNullOrEmpty(title))
            {
                posts = posts.Where(x => x.Title.Equals(title));
            }

            if (sortOrder.Equals("oldest"))
            {
                return posts.OrderBy(x => x.CreatedAt).ToList();
            }
            return posts.OrderByDescending(x => x.CreatedAt).ToList();
        }
    }
}